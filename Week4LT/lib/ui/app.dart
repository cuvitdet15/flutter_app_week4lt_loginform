import 'package:flutter/material.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Login me",
      home: Scaffold(
        appBar: AppBar(title: Text('Login')),
        body: LoginScreen(),
      ),
    );
  }

}

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginState();
  }

}

class LoginState extends State<StatefulWidget> {
  final formKey = GlobalKey<FormState>();
  late String emailAddress;
  late String password;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Form(
        key: formKey,
        child: Column(
          children: [
            fieldEmailAddress(),
            Container(margin: EdgeInsets.only(top: 20.0),),
            fieldPassword(),
            Container(margin: EdgeInsets.only(top: 40.0),),
            loginButton()
          ],
        ),
      )

    );
  }

  Widget fieldEmailAddress() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        icon: Icon(Icons.person),
        labelText: 'Email address'
      ),
      validator: (value) {
        if (!value!.contains('@') || !value!.contains(new RegExp(r'[.]'))) {
          return 'Please input valid email.';
        }
        return null;
      },
      onSaved: (value) {
        emailAddress = value as String;
      },
    );
  }

  Widget fieldPassword() {
    return TextFormField(
      obscureText: true,
      decoration: InputDecoration(
        icon: Icon(Icons.password),
        labelText: 'Password'
      ),
      validator: (value) {
        bool haslowercase = false;
        bool hasuppercase = false;
        bool hasspecialcase = false;
        if (value!.length < 8) {
          return 'Password must be at least 8 characters.';
        }

          if (!value.contains(new RegExp(r'^(?=.*?[A-Z])'))) {
            hasuppercase = true;
            return 'Password must has at least 1 uppercase.';
          }
          if (!value.contains(new RegExp(r'^(?=.*?[a-z])'))){
            haslowercase = true;
            return 'Password must has at least 1 lowercase.';
          }
          if (!value.contains(new RegExp(r'[!@#$%^&*(),.?":{}|<>]'))){
             hasspecialcase = true;
             return 'Password must has at least 1 specialcase.';
          }

      },
      onSaved: (value) {
        password = value as String;
      },
    );
  }

  Widget loginButton() {
    return ElevatedButton(
        onPressed: () {
          if (formKey.currentState!.validate()) {
            // Call API Authentication from Backend
            formKey.currentState!.save();
            print('$emailAddress, Demo only: $password');
          }
        },
        child: Text('Login')
    );
  }
}